package com.meli.melidemo.models;

public class MLReview {
    public String id;
    public int likes;
    public int dislikes;
    public String title;
    public String content;
    public int rate;
}
