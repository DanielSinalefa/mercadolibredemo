package com.meli.melidemo.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.meli.melidemo.ItemDetailActivity;
import com.meli.melidemo.ItemDetailFragment;
import com.meli.melidemo.ProductListActivity;
import com.meli.melidemo.R;
import com.meli.melidemo.databinding.ItemListContentBinding;
import com.meli.melidemo.models.MLPicture;
import com.meli.melidemo.models.MLProduct;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {
    private final List<MLPicture> mValues;
    private Context adapterContext;
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //TODO open fullscreen
        }
    };

    public ImageAdapter(List<MLPicture> items, Context mContext) {
        mValues = items;
        adapterContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_picture, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ImageAdapter.ViewHolder holder, int position) {
        MLPicture picture = mValues.get(position);
        Picasso.get().load(picture.secure_url).error(R.drawable.ic_launcher_background).into(holder.mImageView);
        holder.mTextview.setText(String.format(adapterContext.getString(R.string.gallery_page),position+1, mValues.size()));
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        final ImageView mImageView;
        final TextView mTextview;
        ViewHolder(View view) {
            super(view);
            mImageView = (ImageView) view.findViewById(R.id.item_image);
            mTextview = (TextView) view.findViewById(R.id.tv_gallery_page);
        }
    }
}
