package com.meli.melidemo;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.SearchView;
import android.widget.TextView;

import com.meli.melidemo.adapters.ProductListAdapter;
import com.meli.melidemo.databinding.ActivityItemListBinding;
import com.meli.melidemo.models.MLCategory;
import com.meli.melidemo.models.MLProduct;
import com.meli.melidemo.services.MercadoLibreEndpoints;
import com.meli.melidemo.services.MercadoLibreService;
import com.meli.melidemo.util.MLConstants;
import com.squareup.picasso.Picasso;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * An activity representing a list of Items. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link ItemDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class ProductListActivity extends AppCompatActivity {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    private ActivityItemListBinding binding;
    private MercadoLibreService mlService;
    private CompositeDisposable compositeDisposable;
    private MLCategory currentCategory;
    private String currentQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityItemListBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        compositeDisposable = new CompositeDisposable();
        Toolbar toolbar = binding.toolbar;
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        if (binding.itemListContainer.itemDetailContainer != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }
        configureRetrofit();
        loadCategories();
    }

    private void loadCategories() {
        compositeDisposable.add(mlService.getCategories()
                .subscribe(this::populateCategoriesMenu, Throwable::printStackTrace));
    }

    private void populateCategoriesMenu(List<MLCategory> mlCategories) {
        ArrayAdapter<MLCategory> adapter =
                new ArrayAdapter<>(
                        this,
                        R.layout.dropdown_categories_popup_item,
                        mlCategories);
        binding.itemListContainer.filledCategoriesDropdown.setAdapter(adapter);
        binding.itemListContainer.filledCategoriesDropdown.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                MLCategory selectedCat =  adapter.getItem(i);
                currentCategory = selectedCat;
                filterProductsByCategory(selectedCat, currentQuery);
            }
        });
    }

    private void filterProductsByCategory(MLCategory selectedCat, String query) {
        String catId = selectedCat != null ? selectedCat.id : null;
        compositeDisposable.add(mlService.getFilteredProducts(catId, query)
                .subscribe(this::updateProductsAdapter, Throwable::printStackTrace));
    }

    private void updateProductsAdapter(List<MLProduct> mlProducts) {
        binding.itemListContainer.itemList.setAdapter(new ProductListAdapter(this, mlProducts, mTwoPane));
    }

    private void configureRetrofit() {
        mlService = new MercadoLibreService();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setQueryHint(getString(R.string.search_title));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                currentQuery = s;
                filterProductsByCategory(currentCategory, currentQuery);
                return true;
            }
        });
        return true;
    }
}