package com.meli.melidemo.models;

public class MLCategory {
    public String id;
    public String name;

    @Override
    public String toString() {
        return name;
    }
}
