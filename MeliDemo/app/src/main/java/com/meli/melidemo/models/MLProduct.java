package com.meli.melidemo.models;

import android.os.Parcel;
import android.os.Parcelable;

public class MLProduct implements Parcelable {
    public String id;
    public String title;
    public int price;
    public String thumbnail;

    protected MLProduct(Parcel in) {
        id = in.readString();
        title = in.readString();
        price = in.readInt();
        thumbnail = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeInt(price);
        dest.writeString(thumbnail);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<MLProduct> CREATOR = new Parcelable.Creator<MLProduct>() {
        @Override
        public MLProduct createFromParcel(Parcel in) {
            return new MLProduct(in);
        }

        @Override
        public MLProduct[] newArray(int size) {
            return new MLProduct[size];
        }
    };
}
