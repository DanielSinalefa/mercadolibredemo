package com.meli.melidemo.models;

import java.util.List;

public class MLItemReviews {
    public float rating_average;
    public List<MLReview> reviews;
    public MLReviewPaging paging;

    public class MLReviewPaging {
        public int total;
        public int limit;
    }
}
