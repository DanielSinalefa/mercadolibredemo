package com.meli.melidemo.models;

import java.util.List;

public class MLItem {
    public static final String CONDITION_NOT_SPECIFIED = "not_specified";
    public static final String CONDITION_NEW = "new";
    public static final String CONDITION_USED = "used";
    public List<MLPicture> pictures;
    public String title;
    public String condition;
    public int sold_quantity;
    public int price;
    public int original_price;
    public int available_quantity;
    public MLItemReviews itemReviews;
    public MLDescription itemDescription;
    public MLAddress seller_address;
    public int seller_id;

    public class MLAddress {
        public MLPlace city;
        public MLPlace state;
        public MLPlace country;
    }

    public class MLPlace {
        public String id;
        public String name;
    }
}
