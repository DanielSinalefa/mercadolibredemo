package com.meli.melidemo.services;

import com.meli.melidemo.models.MLCategory;
import com.meli.melidemo.models.MLDescription;
import com.meli.melidemo.models.MLItem;
import com.meli.melidemo.models.MLItemReviews;
import com.meli.melidemo.models.MLSearchResult;
import com.meli.melidemo.models.MLUser;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MercadoLibreEndpoints {
    @GET("/sites/{Site_id}/categories")
    Observable<List<MLCategory>> listCategories(@Path("Site_id") String site);

    @GET("sites/{Site_id}/search")
    Observable<MLSearchResult> getProductsByCategory(@Path("Site_id") String site,
                                                     @Query("category") String categoryId,
                                                     @Query("q")String query);

    @GET("items/{Item_id}")
    Observable<MLItem> getItem(@Path("Item_id") String itemId);

    @GET("reviews/item/{Item_id}")
    Observable<MLItemReviews> getItemReviews(@Path("Item_id") String itemId);

    @GET("users/{User_id}")
    Observable<MLUser> getUserInformation(@Path("User_id") String userId);

    @GET("items/{Item_id}/description")
    Observable<MLDescription> getItemDescription(@Path("Item_id") String itemId);


}
