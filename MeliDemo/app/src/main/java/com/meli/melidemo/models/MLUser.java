package com.meli.melidemo.models;

public class MLUser {
    public String id;
    public MLReputation seller_reputation;

    public class MLReputation {
       public String level_id;
       public String power_seller_status;
       public MLUserTransactions transactions;
    }

    public class MLUserTransactions {
        public int canceled;
        public int completed;
    }
}

