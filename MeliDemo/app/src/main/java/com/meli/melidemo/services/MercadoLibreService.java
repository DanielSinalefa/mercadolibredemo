package com.meli.melidemo.services;

import com.meli.melidemo.models.MLCategory;
import com.meli.melidemo.models.MLItem;
import com.meli.melidemo.models.MLProduct;
import com.meli.melidemo.models.MLSearchResult;
import com.meli.melidemo.models.MLUser;
import com.meli.melidemo.util.MLConstants;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.internal.operators.observable.ObservableCombineLatest;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class MercadoLibreService {
    private MercadoLibreEndpoints mlEndpoints;

    public MercadoLibreService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.mercadolibre.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        mlEndpoints = retrofit.create(MercadoLibreEndpoints.class);
    }

    public Observable<List<MLCategory>> getCategories() {
        return mlEndpoints.listCategories(MLConstants.MLCOLOMBIA)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnError(Throwable::printStackTrace);
    }

    public Observable<List<MLProduct>> getFilteredProducts(String categoryId, String query) {
        return mlEndpoints.getProductsByCategory(MLConstants.MLCOLOMBIA, categoryId, query)
                .map(mlSearchResult -> mlSearchResult.results)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnError(Throwable::printStackTrace);
    }

    public Observable<MLItem> getItem(String itemId) {
        return Observable.zip(mlEndpoints.getItem(itemId), mlEndpoints.getItemReviews(itemId), mlEndpoints.getItemDescription(itemId),
                (item,itemReviews, itemDescription) -> {
                    item.itemReviews = itemReviews;
                    item.itemDescription = itemDescription;
                    return item;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnError(Throwable::printStackTrace);
    }
    public Observable<MLUser> getUser(String userId) {
        return mlEndpoints.getUserInformation(userId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnError(Throwable::printStackTrace);
    }
}
