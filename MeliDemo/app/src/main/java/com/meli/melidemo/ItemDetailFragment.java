package com.meli.melidemo;

import android.app.Activity;
import android.graphics.Paint;
import android.os.Bundle;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.meli.melidemo.adapters.ImageAdapter;
import com.meli.melidemo.databinding.ItemDetailBinding;
import com.meli.melidemo.models.MLItem;
import com.meli.melidemo.models.MLProduct;
import com.meli.melidemo.models.MLUser;
import com.meli.melidemo.services.MercadoLibreService;

import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Locale;

import io.reactivex.disposables.CompositeDisposable;

import static com.meli.melidemo.adapters.ProductListAdapter.setPriceText;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ProductListActivity}
 * in two-pane mode (on tablets) or a {@link ItemDetailActivity}
 * on handsets.
 */
public class ItemDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    private MLProduct currentProduct;
    private ItemDetailBinding binding;
    private CompositeDisposable compositeDisposable;
    private MLItem currentItem;
    private MercadoLibreService service;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        compositeDisposable = new CompositeDisposable();
        if (getArguments().containsKey(ARG_ITEM_ID)) {
            currentProduct = getArguments().getParcelable(ARG_ITEM_ID);

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(currentProduct.title);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        binding = ItemDetailBinding.inflate(inflater,container,false);
        binding.rvItemPictures.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        service = new MercadoLibreService();
        View view = binding.getRoot();
        loadProduct();
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }

    private void loadProduct() {
        compositeDisposable.add(service.getItem(currentProduct.id)
                .subscribe(this::handleItem, Throwable::printStackTrace));
    }

    private void handleItem(MLItem mlItem) {
        currentItem = mlItem;
        loadPictures();
        loadSeller();
        binding.setItem(currentItem);
    }

    private void loadSeller() {
        compositeDisposable.add(service.getUser(currentItem.seller_id+"")
                .subscribe(this::handleSeller, Throwable::printStackTrace));
    }

    private void handleSeller(MLUser mlUser) {
        if(mlUser != null && mlUser.seller_reputation != null) {
            if(mlUser.seller_reputation.power_seller_status != null) {
                binding.tvSellerStatus.setText(String.format(getString(R.string.seller_status_format), mlUser.seller_reputation.power_seller_status));
            } else {
                binding.tvSellerStatus.setVisibility(View.GONE);
            }
            binding.tvSellerLevel.setText(mlUser.seller_reputation.level_id);
            if(mlUser.seller_reputation.transactions != null) {
                binding.tvSellerCompletedTransactions.setText(mlUser.seller_reputation.transactions.completed+"");
            }
            else{
                binding.tvSellerCompletedTransactions.setVisibility(View.GONE);
            }
        }

    }

    private void loadPictures() {
        ImageAdapter imageAdapter = new ImageAdapter(currentItem.pictures, getContext());
        binding.rvItemPictures.setAdapter(imageAdapter);
    }

    @BindingAdapter({"itemCondition","soldQuantity","soldFormat"})
    public static void setItemState(TextView view, String condition, int count, String format) {
        String state = "";
        Boolean hasCondition = false;
        if(condition != null && !condition.isEmpty() && !condition.equals(MLItem.CONDITION_NOT_SPECIFIED)){
            hasCondition = true;
            if(condition.equals(MLItem.CONDITION_NEW)){
                state += view.getResources().getString(R.string.item_state_new);
            }
            else if(condition.equals(MLItem.CONDITION_USED)){
                state += view.getResources().getString(R.string.item_state_used);
            }
        }
        if(count > 0) {
            state += hasCondition ? " - " : "";
            state += String.format(format,count);
        }
        view.setText(state);

    }

    @BindingAdapter({"reviewsFormat","reviewsCount"})
    public static void setReviewsCount(TextView view, String format, int count) {
        if(count == 0) return;
        view.setText(String.format(format,count));
    }

    @BindingAdapter({"sellerAddress", "addressFormat"})
    public static void setItemAddress(TextView view, MLItem.MLAddress address, String format) {
        if(address != null && address.city != null && address.state != null) {
            view.setText(String.format(format, address.city.name, address.state.name));
        }
    }
    @BindingAdapter({"price", "originalPrice"})
    public static void setOriginalPrice(TextView view, int price, int originalPrice) {
        if(originalPrice > 0 && price != originalPrice) {
            view.setVisibility(View.VISIBLE);
            setPriceText(view, originalPrice);
            view.setPaintFlags(view.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        else {
            view.setVisibility(View.GONE);
        }
    }

    @BindingAdapter({"price", "offPrice", "offFormat"})
    public static void setOffPrice(TextView view, int price, int originalPrice, String offFormat) {
        if(originalPrice > 0 && price != originalPrice) {
            view.setVisibility(View.VISIBLE);
            double value = ((double)originalPrice-price)/originalPrice;
            int perc = (int) (value*100);
            view.setText(String.format(offFormat,perc));
        }
        else {
            view.setVisibility(View.GONE);
        }
    }
}