package com.meli.melidemo.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.meli.melidemo.ItemDetailActivity;
import com.meli.melidemo.ItemDetailFragment;
import com.meli.melidemo.ProductListActivity;
import com.meli.melidemo.R;
import com.meli.melidemo.databinding.ItemListContentBinding;
import com.meli.melidemo.models.MLProduct;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class ProductListAdapter
        extends RecyclerView.Adapter<ProductListAdapter.ViewHolder> {

    private final ProductListActivity mParentActivity;
    private final List<MLProduct> mValues;
    private final boolean mTwoPane;
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            MLProduct item = (MLProduct) view.getTag();
            if (mTwoPane) {
                Bundle arguments = new Bundle();
                arguments.putParcelable(ItemDetailFragment.ARG_ITEM_ID, item);
                ItemDetailFragment fragment = new ItemDetailFragment();
                fragment.setArguments(arguments);
                mParentActivity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.item_detail_container, fragment)
                        .commit();
            } else {
                Context context = view.getContext();
                Intent intent = new Intent(context, ItemDetailActivity.class);
                intent.putExtra(ItemDetailFragment.ARG_ITEM_ID, item);

                context.startActivity(intent);
            }
        }
    };

    public ProductListAdapter(ProductListActivity parent,
                              List<MLProduct> items,
                              boolean twoPane) {
        mValues = items;
        mParentActivity = parent;
        mTwoPane = twoPane;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(parent.getContext());
        ItemListContentBinding itemBinding =
                ItemListContentBinding.inflate(layoutInflater, parent, false);
        return new ViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        MLProduct product = mValues.get(position);
        holder.bind(product);
        holder.itemView.setTag(mValues.get(position));
        holder.itemView.setOnClickListener(mOnClickListener);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemListContentBinding binding;

        ViewHolder(ItemListContentBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(MLProduct item) {
            binding.setProduct(item);
            binding.executePendingBindings();
        }
    }

    @BindingAdapter({"imageUrl", "error"})
    public static void loadImage(ImageView view, String url, Drawable error) {
        String aUrl = url.replace("http", "https");
        Picasso.get().load(aUrl).error(error).into(view);
    }

    @BindingAdapter("priceText")
    public static void setPriceText(TextView view, int price) {
        String COUNTRY = "CO";
        String LANGUAGE = "es";
        NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale(LANGUAGE, COUNTRY));
        nf.setMaximumFractionDigits(0);
        String str = nf.format(price);
        view.setText(str);
    }
}